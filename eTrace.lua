if not EventTraceFrame then
	UIParentLoadAddOn("Blizzard_DebugTools")
end

local function addArgs(args, index, ...)
	for i = 1, select("#", ...) do
		if not args[i] then
			args[i] = {}
		end
		args[i][index] = select(i, ...)
	end
end
 
EventTraceFrame:HookScript("OnEvent", function(self, event)
	if event == "COMBAT_LOG_EVENT_UNFILTERED" and not self.ignoredEvents[event] and self.events[self.lastIndex] == event then
		addArgs(self.args, self.lastIndex, CombatLogGetCurrentEventInfo())
	end
end)