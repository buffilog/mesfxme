local addonName, addonTable = ...

meSFXme_OptionsPanel:RegisterEvent("ADDON_LOADED");

local soundChannels = { "Master", "SFX", "Music", "Ambience", "Dialog" };

function meSFXme_OptionsPanel:OnEvent(event, arg1, ...)
	if (event == "ADDON_LOADED" and arg1 == "meSFXme") then
		if (not meSFXme_Settings) then
			meSFXme_Settings = {};
			meSFXme_Settings.SoundChannel = "Master";
		end
		meSFXme_AddOptionsPanel(meSFXme_OptionsPanel);
		meSFXme_SoundChannelDropdown_RegisterScripts();
	end
end

function meSFXme_AddOptionsPanel(panel) 
	panel.name = 'meSFXme';
	panel.okay = function(self) end;
	panel.cancel = function(self) end;
	InterfaceOptions_AddCategory(panel);
end

function meSFXme_SoundChannelDropdown_RegisterScripts()
	UIDropDownMenu_SetWidth(meSFXme_SoundChannelDropdown, 200);
	meSFXme_SoundChannelDropdown_OnLoad(meSFXme_SoundChannelDropdown);
	meSFXme_SoundChannelDropdown:SetScript("OnLoad", meSFXme_SoundChannelDropdown_OnLoad);
	meSFXme_SoundChannelDropdown:SetScript("OnClick", meSFXme_SoundChannelDropdown_OnClick);
end

function meSFXme_SoundChannelDropdown_OnLoad(self)
	UIDropDownMenu_Initialize(self, meSFXme_SoundChannelDropdown_Populate);
end

function meSFXme_SoundChannelDropdown_Populate(dropDown)
	UIDropDownMenu_SetText(dropDown, meSFXme_Settings.SoundChannel);
	local info = UIDropDownMenu_CreateInfo();
	for _, v in pairs(soundChannels) do 
		info.text = v;
		info.value = v;
		info.checked = meSFXme_Settings.SoundChannel == v and 1 or nil;
		info.func = function(self, arg1, arg2, checked)
			meSFXme_Settings.SoundChannel = self:GetText();
			addonTable[1] = meSFXme_Settings.SoundChannel;
			UIDropDownMenu_SetSelectedName(dropDown, self:GetText(), true);
		end;
		UIDropDownMenu_AddButton(info);
	end
end

function meSFXme_SoundChannelDropdown_OnClick(self)
	ToggleDropDownMenu(1, nil, self, "meSFXme_SoundChannelDropdown", 0, 0);
end

meSFXme_OptionsPanel:SetScript("OnEvent", meSFXme_OptionsPanel.OnEvent);


--HITMARKER SOUND EFFECT:


local playerName = UnitName("player");

local frame = CreateFrame("FRAME");
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");

local function eventHandler(event, ...)

	local eventInfo = {CombatLogGetCurrentEventInfo()};
	local subevent = eventInfo[2];
	local caster = eventInfo[5];
	local spell = eventInfo[13];
	
	if((subevent == "RANGE_DAMAGE" or subevent == "SPELL_DAMAGE") and caster == playerName)then
	
		--print("event happened: " .. subevent, caster, spell);
	
		if(string.find(spell, "shot") or string.find(spell, "Shot")) then
				
			--print("spell with the word shot detected: " .. spell);
			
			PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\hitmarker.ogg", meSFXme_Settings.SoundChannel);
		
		end
	
	end

end

frame:SetScript("OnEvent", eventHandler);


--CRITICAL HIT SOUND EFFECT:


local playerGUID = UnitGUID("player")

local frame = CreateFrame("FRAME")
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

--the game's API passes ciritical hit info weird, so a special function handler is needed for passing the arguments to the event handler:
frame:SetScript("OnEvent", function(self, event)
	-- pass a variable number of arguments
	self:OnEvent(event, CombatLogGetCurrentEventInfo())
end)

function frame:OnEvent(event, ...)
	local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
	local spellId, spellName, spellSchool
	local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand

	if subevent == "SWING_DAMAGE" then
		amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
	elseif subevent == "SPELL_DAMAGE" then
		spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
	end

	if critical and sourceGUID == playerGUID then --it was a crit!
		
		PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\MinecraftCrit.ogg", meSFXme_Settings.SoundChannel);
	
	end
end


--FALL DAMAGE SOUND EFFECT:


local playerName = UnitName("player");

local frame = CreateFrame("FRAME");
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");

local function eventHandler(self, event, ...)

	local eventInfo = {CombatLogGetCurrentEventInfo()};
	local subevent = eventInfo[2];
	local target = eventInfo[9];
	local damageType = eventInfo[12];

	--print(subevent);
	--print(damageType);

	if(subevent == "ENVIRONMENTAL_DAMAGE" and damageType == "Falling" and target == playerName)then
		
		PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\LetsJustJumpIntoIt.ogg", meSFXme_Settings.SoundChannel);
		--print("fell");
		
	end

end

frame:SetScript("OnEvent", eventHandler);


-- EATING SOUND EFFECT:


local frame = CreateFrame("FRAME");
frame:RegisterEvent("UNIT_AURA");

local function eventHandler(self, event, ...)
	
	--print("" .. UnitAura("player", 1));

	for i=1,40 do
	
		local name,_,_,_,_,_,source = UnitAura("player", i);
		
		if(name == "Food" and source == "player") then
			
			--print("eating");
			
			PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\MinecraftEatingSound.ogg", meSFXme_Settings.SoundChannel);
			
		end
	
	end

end

frame:SetScript("OnEvent", eventHandler);


-- EPIC OR GREATER LOOT SOUND EFFECT:


local frame = CreateFrame("FRAME");
frame:RegisterEvent("LOOT_READY");

local playerName = UnitName("player");

local function eventHandler(chatMessage, looter, ...)

	lootIcon, lootName, lootQuantity, currencyID, lootQuality, locked, isQuestItem, questID, isActive = GetLootSlotInfo(1);
	
	--print(lootName);
	
	i=1;
	while(lootName ~= nil) do
			
		if(lootQuality >= 4) then
	
			--print("DAANG " .. lootName .. " look good!");
	
			PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\DangThatLookGood.ogg", meSFXme_Settings.SoundChannel);
	
		end
	
		lootIcon, lootName, lootQuantity, currencyID, lootQuality, locked, isQuestItem, questID, isActive = GetLootSlotInfo(i+1);

		i=i+1;
		
	end


end

frame:SetScript("OnEvent", eventHandler);


-- PORTAL SOUND EFFECT:


local portalSpells = {"Portal: Boralus", "Portal: Dazar'alor", "Portal: Stormshield", "Portal: Warspear", "Portal: Vale of Eternal Blossoms", "Portal: Tol Barad", "Portal: Dalaran - Broken Isles", "Ancient Portal: Dalaran", "Portal: Dalaran - Northrend", "Portal: Shattrath", "Portal: Stonard", "Portal: Darnassus", "Portal: Exodar", "Portal: Ironforge", "Portal: Orgrimmar", "Portal: Silvermoon", "Portal: Stormwind", "Portal: Theramore", "Portal: Thunder Bluff", "Portal: Undercity"};

local frame = CreateFrame("FRAME");
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");

local function eventHandler(self, event, ...)

	local eventInfo = {CombatLogGetCurrentEventInfo()};
	
	local subevent = eventInfo[2];
	
	if(subevent == "SPELL_CAST_SUCCESS") then
	
		local spellName = eventInfo[13];
		
		--print("spell cast succes of spell: " .. spellName);
		
		for i=1,20 do
		
			if(spellName == portalSpells[i]) then
			
				--print("portal was cast?");
				
				PlaySoundFile("Interface\\Addons\\meSFXme\\Sounds\\MinecraftNetherPortal.ogg", meSFXme_Settings.SoundChannel);
			
			end
		
		end
	
	end

end

frame:SetScript("OnEvent", eventHandler);